﻿using ServicoOrdenacao.Domain.Entities;
using ServicoOrdenacao.Infra.CrossCutting.ExceptionManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ServicoOrdenacao.Infra.Data
{
    public static class Utils
    {
        public static T CastFileToJsonObject<T>(string path)
        {

            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }

        }

        public static string CreateQuery(List<Ordenation> ordenations)
        {
            try
            {
                var query = string.Empty;

                var ordem = ordenations.GroupBy(x => x.Order);

                for (int i = 0; i < ordenations.Count; i++)
                {
                    query += string.Format("{0} {1}", ordenations[i].Attribute, ordenations[i].Order);

                    if (i < ordenations.Count - 1)
                        query += ",";
                }

                return query;
            }
            catch (ArgumentNullException ex)
            {
                throw new OrdenationException("Erro ao criar query de ordenação", ex);
            }           

        }
    }
}
