﻿using ServicoOrdenacao.Domain.Entities;
using ServicoOrdenacao.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq.Dynamic;
using System.Linq;
using ServicoOrdenacao.Infra.CrossCutting.ExceptionManager;
using ServicoOrdenacao.Domain.Aggregate;

namespace ServicoOrdenacao.Infra.Data.Repository
{
    public class BookRepository : RepositoryBase<Book>, IBookRepository
    {
        public List<Book> Order(OrderBooks orderBooks)
        {
            try
            {
                var order = Utils.CreateQuery(orderBooks.Ordenations);
                return orderBooks.Books.AsQueryable().OrderBy(order).ToList();
            }
            catch (OrdenationException ex)
            {
                throw new OrdenationException(ex.Message, ex);
            }
        }
        
        public List<Book> Order(List<Book> books)
        {
            try
            {
                return books;
            }
            catch (OrdenationException ex)
            {
                throw new OrdenationException(ex.Message, ex);
            }
        }

        public List<Book> GetAllOrdered()
        {
            var retorno = Utils.CastFileToJsonObject<List<Book>>(string.Concat(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["BookConfiguration"]));
            return retorno;
        }
    }
}
