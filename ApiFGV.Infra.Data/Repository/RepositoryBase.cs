﻿using ServicoOrdenacao.Domain.Interfaces;
using ServicoOrdenacao.Domain.Interfaces.Repositories;
using ServicoOrdenacao.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacao.Infra.Data.Repository
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected ServicoOrdenacaoContext ctx = new ServicoOrdenacaoContext();
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        IList<TEntity> IRepositoryBase<TEntity>.GetAll()
        {
            return ctx.Set<TEntity>().ToList();
        }

        TEntity IRepositoryBase<TEntity>.GetById(int id)
        {
            return ctx.Set<TEntity>().Find(id);
        }
    }
}
