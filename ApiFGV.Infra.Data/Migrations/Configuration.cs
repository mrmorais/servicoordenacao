namespace ServicoOrdenacao.Infra.Data.Migrations
{
    using Domain.Entities;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity.Migrations;
    using System.IO;

    internal sealed class Configuration : DbMigrationsConfiguration<ServicoOrdenacao.Infra.Data.Context.ServicoOrdenacaoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Context.ServicoOrdenacaoContext context)
        {
            //Trecho comentado pois n�o havia necessidade de persistencia no banco de dados
            //Caso queria testar a persistencia, apenas descomente as linhas abaixo.

            //var books = Utils.CastFileToJsonObject<List<Book>>(string.Concat(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["BookConfiguration"]));
            //books.ForEach(z =>
            //context.Books.AddOrUpdate(z)
            //);
        }
    }
}
