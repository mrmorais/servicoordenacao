﻿using ServicoOrdenacao.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacao.Infra.Data.EntityConfig
{
    /**Classe de mapeamento da entidade Book na ORM**/
    
    public class BookConfiguration : EntityTypeConfiguration<Book>
    {

        public BookConfiguration()
        {
            HasKey(x => x.BookId);

            Property(x => x.Title)
                .IsRequired();

            Property(x => x.AuthorName)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
