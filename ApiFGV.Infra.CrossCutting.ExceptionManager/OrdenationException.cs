﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacao.Infra.CrossCutting.ExceptionManager
{
    [Serializable]
    public class OrdenationException : Exception
    {
        public OrdenationException()
        {

        }
        public OrdenationException(string mensagem) : base (mensagem)
        {

        }

        public OrdenationException(string mensagem, Exception ex) : base(mensagem, ex)
        {
            
        }

        public OrdenationException(string mensagem, ArgumentNullException ex) : base(mensagem, ex)
        {

        }
    }
}
