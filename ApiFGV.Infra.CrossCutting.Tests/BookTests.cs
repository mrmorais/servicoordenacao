﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServicoOrdenacao.Infra.Data.Repository;
using ServicoOrdenacao.Infra.Data;
using System.Collections.Generic;
using ServicoOrdenacao.Domain.Entities;
using ServicoOrdenacao.Infra.CrossCutting.ExceptionManager;
using System;
using ServicoOrdenacao.Domain.Aggregate;
using System.Configuration;

namespace ServicoOrdenacao.Infra.CrossCutting.Tests
{
    [TestClass]
    public class BookTests
    {
        string bookConfigPath = "../../../Configuration/BookConfiguration.json";
        [TestMethod]
        public void RetornoTituloAscendente()
        {
            var ordenation = new List<Ordenation>() { new Ordenation { Attribute = "Title", Order = "asc" } };
            var books = Utils.CastFileToJsonObject<List<Book>>(string.Concat(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["BookConfiguration"]));
            var orderBooks = new OrderBooks() { Books = books, Ordenations = ordenation };

            books = new BookRepository().Order(orderBooks);

            Assert.IsNotNull(books);
        }

        [TestMethod]
        public void RetornoTituloAscendenteAutorDescendente()
        {
            var ordenation = new List<Ordenation>() { new Ordenation { Attribute = "Title", Order = "asc" },
            new Ordenation { Attribute = "AuthorName", Order = "desc"} };

            var books = Utils.CastFileToJsonObject<List<Book>>(string.Concat(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["BookConfiguration"]));

            var orderBooks = new OrderBooks() { Books = books, Ordenations = ordenation };

            books = new BookRepository().Order(orderBooks);

            Assert.IsNotNull(books);
        }


        [TestMethod]
        public void RetornoEdicaoDescendenteAutorDescendenteTituloDescendente()
        {
            var ordenation = new List<Ordenation>() {
            new Ordenation { Attribute = "EditionYear", Order = "desc" },
            new Ordenation { Attribute = "AuthorName", Order = "desc"},
            new Ordenation { Attribute = "Title", Order = "asc"}
            };

            var books = Utils.CastFileToJsonObject<List<Book>>(string.Concat(AppDomain.CurrentDomain.BaseDirectory, bookConfigPath));
            var orderBooks = new OrderBooks() { Books = books, Ordenations = ordenation };

            books = new BookRepository().Order(orderBooks);

            Assert.IsNotNull(books);
        }
        
        [TestMethod]
        [ExpectedException(typeof(OrdenationException))]
        public void RetornoOrdenacaoException()
        {

            var books = Utils.CastFileToJsonObject<List<Book>>(string.Concat(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["BookConfiguration"]));
            var orderBooks = new OrderBooks() { Books = books, Ordenations = null };

            books = new BookRepository().Order(orderBooks);
        }

        [TestMethod]
        public void RetornoConjuntoVazio()
        {
           var books = new BookRepository().Order(new List<Book>());
             Assert.IsTrue(books.Count ==0);
        }

    }
}
