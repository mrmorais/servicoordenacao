﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacao.Domain.Entities
{
    public class Ordenation
    {
        public string Attribute { get; set; }
        public string Order { get; set; }
    }
}
