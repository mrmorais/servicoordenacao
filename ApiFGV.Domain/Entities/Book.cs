﻿using ServicoOrdenacao.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServicoOrdenacao.Domain.Entities
{
    public class Book
    {
        public int BookId { get; set; }
        public string AuthorName { get; set; }
        public int EditionYear { get; set; }
        public string Title { get; set; }
    }
}
