﻿
using ServicoOrdenacao.Domain.Entities;
using System.Collections.Generic;

namespace ServicoOrdenacao.Domain.Aggregate
{
    public class OrderBooks
    {
        public List<Book> Books { get; set; }
        public List<Ordenation> Ordenations { get; set; }
    }
}
