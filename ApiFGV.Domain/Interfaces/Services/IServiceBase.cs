﻿using System.Collections.Generic;

namespace ServicoOrdenacao.Domain.Interfaces.Services
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        IList<TEntity> GetAll();
        TEntity GetById(int id);
        void Dispose();
    }
}
