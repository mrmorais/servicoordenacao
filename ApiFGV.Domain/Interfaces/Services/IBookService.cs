﻿using ServicoOrdenacao.Domain.Aggregate;
using ServicoOrdenacao.Domain.Entities;
using System.Collections.Generic;

namespace ServicoOrdenacao.Domain.Interfaces.Services
{
    public interface IBookService : IServiceBase<Book>
    {
        List<Book> Order(OrderBooks orderBooks);
        List<Book> GetAllOrdered();
    }
}
