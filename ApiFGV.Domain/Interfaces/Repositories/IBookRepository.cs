﻿using ServicoOrdenacao.Domain.Aggregate;
using ServicoOrdenacao.Domain.Entities;
using System.Collections.Generic;

namespace ServicoOrdenacao.Domain.Interfaces.Repositories
{
    public interface IBookRepository : IRepositoryBase<Book>
    {
        List<Book> Order(OrderBooks orderBooks);
        List<Book> GetAllOrdered();
    }
}
