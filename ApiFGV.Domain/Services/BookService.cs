﻿using ServicoOrdenacao.Domain.Entities;
using ServicoOrdenacao.Domain.Interfaces.Repositories;
using ServicoOrdenacao.Domain.Interfaces.Services;
using ServicoOrdenacao.Domain.Services;
using System.Collections.Generic;
using System;
using ServicoOrdenacao.Domain.Aggregate;

namespace ServicoOrdenacao.Application.Services
{
    public class BookService : ServiceBase<Book>, IBookService
    {
       private readonly IBookRepository _bookRepository;

        public BookService(IBookRepository bookRepository)
            : base(bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public List<Book> GetAllOrdered()
        {
            return _bookRepository.GetAllOrdered();
        }

        public List<Book> Order(OrderBooks orderBooks)
        {
            return _bookRepository.Order(orderBooks);
        }
    }
}
