﻿using ServicoOrdenacao.Domain.Interfaces.Repositories;
using ServicoOrdenacao.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace ServicoOrdenacao.Domain.Services
{
    public class ServiceBase<TEntity> : IDisposable, IServiceBase<TEntity> where TEntity : class
    {
        private readonly IRepositoryBase<TEntity> _repository;

        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        IList<TEntity> IServiceBase<TEntity>.GetAll()
        {
            return _repository.GetAll();
        }

        TEntity IServiceBase<TEntity>.GetById(int id)
        {
            return _repository.GetById(id);
        }
    }
}
