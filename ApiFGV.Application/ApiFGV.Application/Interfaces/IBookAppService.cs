﻿using ServicoOrdenacao.Domain.Aggregate;
using ServicoOrdenacao.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicoOrdenacao.Application.Interfaces
{
    public interface IBookAppService : IAppServiceBase<Book>
    {
        List<Book> Order(OrderBooks orderBooks);
        List<Book> GetAllOrdered();
    }
}
