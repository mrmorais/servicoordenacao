﻿using ServicoOrdenacao.Application.Interfaces;
using ServicoOrdenacao.Domain.Entities;
using ServicoOrdenacao.Domain.Interfaces.Services;
using System.Collections.Generic;
using System;
using ServicoOrdenacao.Domain.Aggregate;

namespace ServicoOrdenacao.Application.Services
{
    public class BookAppService : AppServiceBase<Book>, IBookAppService
    {
        private readonly IBookService _bookService;
      
        public List<Book> Order(OrderBooks orderBooks)
        {
            return _bookService.Order(orderBooks);
        }

        public List<Book> GetAllOrdered()
        {
           return  _bookService.GetAllOrdered();
        }

        public BookAppService(IBookService bookService) 
            : base(bookService)
        {
            _bookService = bookService;
        }
    }
}
