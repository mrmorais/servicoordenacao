﻿using ServicoOrdenacao.Application.Interfaces;
using ServicoOrdenacao.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace ServicoOrdenacao.Application.Services
{
    public class AppServiceBase<TEntity> : IDisposable, IAppServiceBase<TEntity> where TEntity : class
    {

        private readonly IServiceBase<TEntity> _serviceBase;
        public AppServiceBase(IServiceBase<TEntity> serviceBase)
        {
            _serviceBase = serviceBase;
        }

        public void Dispose()
        {
            _serviceBase.Dispose();
        }

        public IList<TEntity> GetAll()
        {
            return  _serviceBase.GetAll();
        }

        public TEntity GetById(int id)
        {
            return _serviceBase.GetById(id);
        }
    }
}
