﻿using System.ComponentModel.DataAnnotations;

namespace ApiFGV.MVC.ViewModels
{
    public class BookViewModel
    {
        [Key]
        public int BookId { get; set; }
        public string AuthorName { get; set; }
        public int EditionYear { get; set; }
        public string Title { get; set; }
    }
}