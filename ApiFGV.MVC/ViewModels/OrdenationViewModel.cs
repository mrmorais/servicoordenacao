﻿using System.ComponentModel.DataAnnotations;

namespace ApiFGV.MVC.ViewModels
{
    public class OrdenationViewModel
    {
        public string Attribute { get; set; }
        public int Order { get; set; }
    }
}