﻿using ApiFGV.Domain.Entities;
using ApiFGV.MVC.ViewModels;
using AutoMapper;

namespace ApiFGV.MVC.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Book, BookViewModel>();
            Mapper.CreateMap<Ordenation, OrdenationViewModel>();
        }
    }
}
