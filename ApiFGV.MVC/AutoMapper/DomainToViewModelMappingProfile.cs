﻿
using ApiFGV.Domain.Entities;
using ApiFGV.MVC.ViewModels;
using AutoMapper;

namespace ApiFGV.MVC.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BookViewModel, Book>();
            Mapper.CreateMap<OrdenationViewModel, Ordenation>();
        }
    }
}
