﻿using ServicoOrdenacao.Application.Interfaces;
using ServicoOrdenacao.Domain.Aggregate;
using ServicoOrdenacao.Domain.Entities;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Mvc;

namespace ServicoOrdenacao.MVC.Controllers
{
    public class  BookController : Controller
    {
        private readonly IBookAppService _bookAppService;

        public BookController(IBookAppService bookAppService)
        {
            _bookAppService = bookAppService;
        }

        public BookController()
        {

        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetBooks()
        {
            return Json(_bookAppService.GetAllOrdered(),JsonRequestBehavior.AllowGet);
        }

        public JsonResult Test()
        {
            var order = new OrderBooks() { Books = new List<Book>() { new Book() { AuthorName = "teste", EditionYear = 2015, Title = "testando", BookId = 4 } }, Ordenations = new List<Ordenation>() { new Ordenation { Attribute = "Title", Order = "Asc" } } };
            return Json(order, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult OrderBooks([FromBody]OrderBooks orderBooks)
        {
            return Json(_bookAppService.Order(orderBooks));
        }
    }
}
